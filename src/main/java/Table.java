
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author TUFGaming
 */
public class Table implements Serializable{

    char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};

    Player playerX;
    Player playerO;
    Player currentPlayer;
    Player winner = null;
    int turn;
    boolean isFinish;

    public Table(Player x, Player o) {
        this.playerX = x;
        this.playerO = o;
        this.currentPlayer = x;
        this.isFinish = false;
        this.turn = 0;
    }

    public void setIsFinish() {
        isFinish = true;
    }
    
    public boolean getsetIsFinish(){
        return isFinish ;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println(" ");
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;

        } else {
            currentPlayer = playerX;
        }
    }

    public boolean setRowCol(int Row, int Col) {
        if(getsetIsFinish()) return false;
        if (table[Row][Col] == '-') {
            table[Row][Col] = currentPlayer.getName();
            turn();
            checkWin();
            return true;
        } else {
            return false;
        }
    }
    public char getRowCol(int Row, int Col){
        return table[Row][Col];
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == table[row][1] && table[row][1] == table[row][2]
                    && table[row][0] != '-') {
                winner = currentPlayer;
                if(winner.name == 'X'){
            playerX.win();
            playerO.lose();
        }else{
            playerX.lose();
            playerO.win();
        }
                setIsFinish();
                break;
            }
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == table[1][col] && table[1][col] == table[2][col]
                    && table[0][col] != '-') {
                winner = currentPlayer;
                if(winner.name == 'X'){
            playerX.win();
            playerO.lose();
        }else{
            playerX.lose();
            playerO.win();
        }
                setIsFinish();
                break;
            }
        }
    }

    public void checkDiagonal() {
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            winner = currentPlayer;
            isFinish = true;
        } else if (table[0][2] == table[1][1] && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            winner = currentPlayer;
            if(winner.name == 'X'){
            playerX.win();
            playerO.lose();
        }else{
            playerX.lose();
            playerO.win();
        }
            setIsFinish();
        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        
        if (turn == 9 && getWinner() == null) {
            setIsFinish();
            winner = null;
            playerX.draw();
            playerO.draw();
        }
    }

    public void turn() {
        turn++;
    }

    public Player getWinner() {
        return winner;
    }
}
